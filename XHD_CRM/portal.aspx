<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1">
    <title></title>
    <script src="lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="css/portal.css" />
    <link href="CSS/core.css" rel="stylesheet" />
    <link href="CSS/Toolbar.css" rel="stylesheet" />
    <script src="JS/portal.js" type="text/javascript"></script>
    <script src="JS/Toolbar.js" type="text/javascript"></script>
    <script src="JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        function a() {
            var cooldrag = CoolDrag.read("cooldrag");
            alert(cooldrag);
        }
        $(function () {
            toolbar();
            //CoolDrag.init('container');
            $.ajax({
                type: "GET",
                url: "../data/C_Sys_portal.ashx", /* 注意后面的名字对应CS的方法名称 */
                data: { Action: 'Get', rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    //alert(obj.constructor); //String 构造函数
                    for (var i = obj.length - 1; i >= 0 ; i--) {
                        var itemstr = "<div style='width: 99%;' class='dragLayer' id='" + obj[i].item_id + "'>";
                        itemstr += "        <div class='dragHeader'>";
                        itemstr += "            <div class='left'>" + obj[i].item_title + "</div>";
                        itemstr += "            <div class='right'>";
                        if (obj[i].item_CanRemove == "1")
                            itemstr += "<div class='close'></div>";
                        itemstr += "            </div>";
                        itemstr += "        </div>";
                        itemstr += "        <div class='content'>";
                        itemstr += "            <div class='main'>window 1</div>";
                        itemstr += "        </div>";
                        itemstr += "        <div class='dragFooter'>";
                        itemstr += "            <div class='right'></div>";
                        itemstr += "        </div>";
                        itemstr += "    </div>";
                        $("#" + obj[i].item_appendto).append($(itemstr));
                        CoolDrag.init('container');
                        CoolDrag.saveCoolDragStatus();
                    }
                }
            });
        })
        function saveCoolDrag(str)
        {
            $.ajax({
                type: "GET",
                url: "../data/C_Sys_portal.ashx", /* 注意后面的名字对应CS的方法名称 */
                data: { Action: 'save', CoolDrag: str, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    
                }
            });
        }
        function toolbar() {
            var toolbar = new Toolbar({
                renderTo: 'toolbar',
                items: [{
                    type: 'button',
                    text: '添加模块',
                    bodyStyle: 'new',
                    useable: 'enabled',
                    handler: function () {
                        add();
                    }
                },'-' , {
                    type: 'button',
                    text: '保存设置',
                    bodyStyle: 'save',
                    useable: 'enabled',
                    handler: function () {
                        add();
                    }
                }
                ],
                active: 'ALL'//激活哪个
            });
            toolbar.render();
        }
        function add() {
            //var itemstr = "<div style='width: 99%;' class='dragLayer' id='win3'>";
            //itemstr += "        <div class='dragHeader'>";
            //itemstr += "            <div class='left'>我的待办111</div>";
            //itemstr += "            <div class='right'></div>";
            //itemstr += "        </div>";
            //itemstr += "        <div class='content'>";
            //itemstr += "            <div class='main'>window 1</div>";
            //itemstr += "        </div>";
            //itemstr += "        <div class='dragFooter'>";
            //itemstr += "            <div class='right'></div>";
            //itemstr += "        </div>";
            //itemstr += "    </div>";
            //$("#leftcontainer").append($(itemstr));
            ////alert($(">div",$(itemstr)).attr("id"));
            //CoolDrag.saveCoolDragStatus();
            //CoolDrag.init('container');
            //$("#win1").remove();
        }
    </script>
</head>
<body style="overflow-y: auto">
    <form id="form1">
        <%--<input type="button"  onclick="a()" value="a" />--%>
        <div id="toolbar"></div>
        <div id="container">
            <div id="leftcontainer">
            </div>

            <div id="centercontainer">
            </div>

            <div id="rightcontainer">
            </div>

        </div>
    </form>
</body>
</html>
